package navis.hust.NavisMana.services;

import navis.hust.NavisMana.model.Device;

import java.io.IOException;
import java.util.Collection;

public interface DeviceService {
    Device create(Device device);

    Collection<Device> list(int limit);

    Device ping(String ipAddress) throws IOException;

    Device get(Long id);

    Device update(Device device);

    Boolean delete(Long id);
}
