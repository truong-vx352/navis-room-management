package navis.hust.NavisMana.services.implementation;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import navis.hust.NavisMana.enumeration.Status;
import navis.hust.NavisMana.model.Device;
import navis.hust.NavisMana.repository.DeviceRepository;
import navis.hust.NavisMana.services.DeviceService;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.InetAddress;
import java.util.Collection;

import static navis.hust.NavisMana.enumeration.Status.DEVICE_ACTIVE;
import static navis.hust.NavisMana.enumeration.Status.DEVICE_DEACTIVE;

@RequiredArgsConstructor
@Service
@Transactional
@Slf4j
public class DeviceServiceImpl implements DeviceService {
    private final DeviceRepository deviceRepository;

    @Override
    public Device create(Device device) {
        log.info("Saving new server: {}", device.getName());
        return deviceRepository.save(device);
    }

    @Override
    public Collection<Device> list(int limit) {
        log.info("log all");
        return deviceRepository.findAll(PageRequest.of(0, limit)).toList();
    }

    @Override
    public Device ping(String ipAddress) throws IOException {
        Device device = deviceRepository.findByIpAddress(ipAddress);

        InetAddress address = InetAddress.getByName(ipAddress);
        device.setStatus(address.isReachable(10000) ? DEVICE_ACTIVE : DEVICE_DEACTIVE);

        deviceRepository.save(device);
        return device;
    }

    @Override
    public Device get(Long id) {
        return deviceRepository.findById(id).get();
    }

    @Override
    public Device update(Device device) {
        return deviceRepository.save(device);
    }

    @Override
    public Boolean delete(Long id) {
        deviceRepository.deleteById((id));
        return Boolean.TRUE;
    }
}
