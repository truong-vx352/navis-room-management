package navis.hust.NavisMana;

import navis.hust.NavisMana.enumeration.Status;
import navis.hust.NavisMana.model.Device;
import navis.hust.NavisMana.repository.DeviceRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class NavisManaApplication {

	public static void main(String[] args) {
		SpringApplication.run(NavisManaApplication.class, args);
	}

	@Bean
	CommandLineRunner run(DeviceRepository deviceRepository) {
		return args -> {
			deviceRepository.save(new Device(null, null, "PC_01", "192.168.0.1", "d9:da:28:8c:29:4d", Status.DEVICE_ACTIVE));
			deviceRepository.save(new Device(null, null, "PC_02", "192.168.0.2", "d9:da:28:8c:29:5d", Status.DEVICE_ACTIVE));
			deviceRepository.save(new Device(null, null, "PC_03", "192.168.0.3", "d9:da:28:8c:29:6d", Status.DEVICE_ACTIVE));
			deviceRepository.save(new Device(null, null, "PC_04", "192.168.0.4", "d9:da:28:8c:29:7d", Status.DEVICE_ACTIVE));
		};
	}
}
