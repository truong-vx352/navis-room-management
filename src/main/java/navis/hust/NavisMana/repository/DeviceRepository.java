package navis.hust.NavisMana.repository;

import navis.hust.NavisMana.model.Device;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DeviceRepository extends JpaRepository<Device, Long> {
    Device findByIpAddress(String ipAddress);
}
