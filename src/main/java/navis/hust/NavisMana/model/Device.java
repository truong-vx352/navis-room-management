package navis.hust.NavisMana.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import navis.hust.NavisMana.enumeration.Status;

import static jakarta.persistence.GenerationType.AUTO;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Device {
    @Id
    @GeneratedValue(strategy = AUTO)
    private Long id;
    private Long classroom_id;
    private String name;
    @Column(unique = true)
    @NotEmpty(message = "Ip address cannot empty")
    private String ipAddress;
    @Column(unique = true)
    private String mac_address;
    private Status status;
}
