package navis.hust.NavisMana.enumeration;

public enum Status {
    DEVICE_ACTIVE("DEVICE_ACTIVE"),
    DEVICE_DEACTIVE("DEVICE_DEACTIVE");
    private final String status;


    Status(String status) {
        this.status = status;
    }

    public String getStatus() {
        return this.status;
    }
}
