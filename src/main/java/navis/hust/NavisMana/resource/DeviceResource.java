package navis.hust.NavisMana.resource;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import navis.hust.NavisMana.enumeration.Status;
import navis.hust.NavisMana.model.Device;
import navis.hust.NavisMana.model.Response;
import navis.hust.NavisMana.services.implementation.DeviceServiceImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Map;

import static java.time.LocalDateTime.now;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping("/device")
@RequiredArgsConstructor
public class DeviceResource {
    private final DeviceServiceImpl deviceService;

    @GetMapping("/")
    public ResponseEntity<Response> getDevice() {
        return ResponseEntity.ok(
                Response.builder().timeStamp(now())
                        .data(Map.of("device", deviceService.list(10)))
                        .message("retrieve list serve")
                        .status(OK)
                        .statusCode(OK.value())
                        .build()
        );
    }

    @GetMapping("/ping/{ipAddress}")
    public ResponseEntity<Response> pingDevice(@PathVariable("ipAddress") String ipAddress) throws IOException {
        Device device = deviceService.ping(ipAddress);
        return ResponseEntity.ok(
                Response.builder().timeStamp(now())
                        .data(Map.of("device", device))
                        .message(device.getStatus() == Status.DEVICE_ACTIVE ? "Ping success" : "Ping failed")
                        .status(OK)
                        .statusCode(OK.value())
                        .build()
        );
    }

    @PostMapping("/save")
    public ResponseEntity<Response> saveDevice(@RequestBody @Valid Device device) {
        return ResponseEntity.ok(
                Response.builder().timeStamp(now())
                        .data(Map.of("device", deviceService.create(device)))
                        .message("device created")
                        .status(CREATED)
                        .statusCode(CREATED.value())
                        .build()
        );
    }
}

